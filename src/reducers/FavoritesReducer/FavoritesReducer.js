const initialState = {
  favorites: [],
};

const FavoritesReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TO_FAVORITES':
      return {
        ...state,
        favorites: state.favorites.concat(action.payload),
      };

    case 'REMOVE_FROM_FAVORITES':
      return {
        ...state,
        favorites: state.favorites.filter(item => action.payload.char_id !== item.char_id),
      };

    default:
      return state;
  }
};

export default FavoritesReducer;
