import { combineReducers } from "redux";
import FavoritesReducer from "./FavoritesReducer/FavoritesReducer";


const rootReducer = combineReducers({
    favoriteReducer: FavoritesReducer
})

export default rootReducer