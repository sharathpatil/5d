import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Image,
  Dimensions,
  FlatList,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import CharacterCard from '../../components/CharacterCard';

const FavoritesView = props => {
  const _renderCardItem = ({item}) => {
    return (
      <CharacterCard
        image={item.img}
        title={item.name}
        subTitle={item.nickname}
        isFav={true}
        onFavPressed={() => {
          props.onFavClicked(item);
        }}
      />
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor="#070707"
        animated={true}
        barStyle="light-content"
      />

      <View style={styles.header}>
        <Text style={styles.headerTitle}>Favourities</Text>

        <TouchableOpacity
          onPress={() => {
            props.goBack();
          }}>
          <Feather name="x" size={24} color="#fff" />
        </TouchableOpacity>
      </View>

      <FlatList
        data={props.favorites}
        renderItem={_renderCardItem}
        showsVerticalScrollIndicator={false}
        numColumns={2}
      />
    </SafeAreaView>
  );
};

export default FavoritesView;

const fontBold = 'Roboto-Bold';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#070707',
  },

  iconStyle: {
    width: 22,
    height: 22,
    marginLeft: 15,
  },

  header: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    backgroundColor: '#070707',
  },

  headerTitle: {
    flex: 1,
    fontSize: 18,
    fontFamily: fontBold,
    color: '#18CA75',
  },
});
