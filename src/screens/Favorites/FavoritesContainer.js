import React from 'react';
import {connect} from 'react-redux';
import FavoritesView from './FavoritesView';
import {removeFromFavorites} from '../../actions/Favorites/FavoritesAction';

const FavoritesContainer = props => {
  const onFavClicked = character => {
    if (props.favorites.some(item => item.char_id == character.char_id)) {
      props.removeCharacterToFav(character);
    }
  };

  const goBack = () => {
    props.navigation.goBack();
  };

  return (
    <FavoritesView
      favorites={props.favorites}
      onFavClicked={onFavClicked}
      goBack={goBack}
    />
  );
};

const mapStateToProps = state => {
  return {
    favorites: state.favoriteReducer.favorites,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    removeCharacterToFav: character => dispatch(removeFromFavorites(character)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesContainer);
