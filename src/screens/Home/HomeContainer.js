import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {BASE_URL} from '../../Constants';
import HomeView from './HomeView';

import {showToast} from '../../components/ShowToast';
import {
  addToFavorites,
  removeFromFavorites,
} from '../../actions/Favorites/FavoritesAction';

const HomeContainer = props => {
  const [characters, setCharacters] = useState([]);
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    getAllCharacters();
    return () => {};
  }, []);

  const getAllCharacters = async () => {
    setLoading(true);

    await axios
      .get(BASE_URL + '/api/characters')
      .then(res => {
        setCharacters(res.data);
        setLoading(false);
      })
      .catch(error => {
        showToast('Something went wrong, try again');
        setLoading(false);
      });
  };

  const onSearchClicked = () => {
    props.navigation.navigate('SearchContainer');
  };

  const onFavorites = () => {
    props.navigation.navigate('FavoritesContainer');
  };

  const onFavClicked = character => {
    if (props.favorites.some(item => item.char_id == character.char_id)) {
      console.log('yes');
      props.removeCharacterToFav(character);
    } else {
      console.log('no');
      props.addCharacterToFav(character);
    }
  };

  const checkIsFav=(character)=>{
    return props.favorites.some(item => item.char_id == character.char_id)
  }

  return (
    <HomeView
      characters={characters}
      onSearchClicked={onSearchClicked}
      onFavClicked={onFavClicked}
      checkIsFav={checkIsFav}
      onFavorites={onFavorites}
      loading={loading}
    />
  );
};

const mapStateToProps = state => {
  return {
    favorites: state.favoriteReducer.favorites,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addCharacterToFav: character => dispatch(addToFavorites(character)),
    removeCharacterToFav: character => dispatch(removeFromFavorites(character)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
