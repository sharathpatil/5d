import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Image,
  Dimensions,
  FlatList,
  ActivityIndicator,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import CharacterCard from '../../components/CharacterCard';

const HomeView = props => {
  const _renderCardItem = ({item, index}) => {
    return (
      <CharacterCard
        image={item.img}
        title={item.name}
        subTitle={item.nickname}
        isFav={props.checkIsFav(item)}
        onFavPressed={() => {
          props.onFavClicked(item);
        }}
      />
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor="#070707"
        animated={true}
        barStyle="light-content"
      />

      <View style={styles.header}>
        <Text style={styles.headerTitle}>The Breaking Bad</Text>

        <TouchableOpacity
          onPress={() => {
            props.onSearchClicked();
          }}>
          <Feather name="search" size={24} color="#fff" />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            props.onFavorites();
          }}>
          <Image
            source={require('../../assets/images/fav-fill.png')}
            style={styles.iconStyle}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>

      {props.loading ? (
        <View style={{margin:20}}>
          <ActivityIndicator color="#18CA75" size="large" />
        </View>
      ) : null}

      <FlatList
        data={props.characters}
        renderItem={_renderCardItem}
        showsVerticalScrollIndicator={false}
        numColumns={2}
      />
    </SafeAreaView>
  );
};

export default HomeView;

const fontBold = 'Roboto-Bold';
const fontLight = 'Roboto-Light';
const fontThin = 'Roboto-Thin';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#070707',
  },

  iconStyle: {
    width: 22,
    height: 22,
    marginLeft: 15,
  },

  header: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
  },

  headerTitle: {
    flex: 1,
    fontSize: 18,
    fontFamily: fontBold,
    color: '#fff',
  },
});
