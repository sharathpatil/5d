import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Image,
  Dimensions,
  FlatList,
  TextInput,
  ActivityIndicator,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import CharacterCard from '../../components/CharacterCard';

const SearchView = props => {
  const _renderCardItem = ({item}) => {
    return (
      <CharacterCard
        image={item.img}
        title={item.name}
        subTitle={item.nickname}
        isFav={props.checkIsFav(item)}
        onFavPressed={() => {
          props.onFavClicked(item);
        }}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor="#070707"
        animated={true}
        barStyle="light-content"
      />

      <View style={styles.header}>
        <TextInput
          placeholder="Search"
          placeholderTextColor="#ABABAB"
          style={styles.searchInput}
          value={props.searchQuery}
          onChangeText={text => {
            props.setSearchQuery(text);
          }}
          onEndEditing={() => {
            console.log('end Editing');
            props.searchCharacter();
          }}
        />

        <TouchableOpacity
          onPress={() => {
            props.goBack();
          }}>
          <Feather name="x" size={24} color="#fff" />
        </TouchableOpacity>
      </View>
      

      {props.loading ? (
        <View style={{margin:20}}>
          <ActivityIndicator color="#18CA75" size="large" />
        </View>
      ) : null}

      {props.isEmpty ? (
        <View style={{marginHorizontal: 15, marginTop: 35}}>
          <Text style={styles.messageText}>No character found</Text>
          <Text style={styles.tryText}>Try gain</Text>
        </View>
      ) : null}

      <FlatList
        data={props.characters}
        renderItem={_renderCardItem}
        showsVerticalScrollIndicator={false}
        numColumns={2}
      />
    </SafeAreaView>
  );
};

export default SearchView;

const fontLight = 'Roboto-Light';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#070707',
  },

  iconStyle: {
    width: 22,
    height: 22,
    marginLeft: 15,
  },

  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 8,
    backgroundColor: '#242424',
  },

  searchInput: {
    flex: 1,
    fontSize: 18,
    fontFamily: fontLight,
    color: '#fff',
  },

  messageText: {
    fontSize: 18,
    fontFamily: fontLight,
    color: '#18CA75',
  },

  tryText: {
    fontSize: 16,
    fontFamily: fontLight,
    color: '#C4C4C4',
    marginTop: 4,
  },
});
