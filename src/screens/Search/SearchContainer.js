import axios from 'axios';
import React, {useState} from 'react';
import {BASE_URL} from '../../Constants';
import SearchView from './SearchView';

import {
  addToFavorites,
  removeFromFavorites,
} from '../../actions/Favorites/FavoritesAction';
import {connect} from 'react-redux';
import { showToast } from '../../components/ShowToast';

const SearchContainer = props => {
  const [searchQuery, setSearchQuery] = useState('');
  const [isEmpty, setIsEmpty] = useState(false);
  const [characters, setCharacters] = useState([]);
  const [loading, setLoading] = useState(false);

  const searchCharacter = async () => {
    if (!searchQuery) return;

    setLoading(true);
    await axios
      .get(BASE_URL + '/api/characters?name=' + searchQuery)
      .then(res => {
        if (res.data.length == 0) {
          setIsEmpty(true);
        } else {
          setIsEmpty(false);
        }

        setLoading(false);
        setCharacters(res.data);
      })
      .catch(error => {
          showToast(error);
          setLoading(false)
      });
  };

  const onFavClicked = character => {
    if (props.favorites.some(item => item.char_id == character.char_id)) {
      console.log('yes');
      props.removeCharacterToFav(character);
    } else {
      console.log('no');
      props.addCharacterToFav(character);
    }
  };

  const checkIsFav = character => {
    return props.favorites.some(item => item.char_id == character.char_id);
  };

  const goBack = () => {
    props.navigation.goBack();
  };

  return (
    <SearchView
      searchQuery={searchQuery}
      setSearchQuery={setSearchQuery}
      characters={characters}
      searchCharacter={searchCharacter}
      goBack={goBack}
      onFavClicked={onFavClicked}
      checkIsFav={checkIsFav}
      isEmpty={isEmpty}
      loading={loading}
    />
  );
};

const mapStateToProps = state => {
  return {
    favorites: state.favoriteReducer.favorites,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addCharacterToFav: character => dispatch(addToFavorites(character)),
    removeCharacterToFav: character => dispatch(removeFromFavorites(character)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchContainer);
