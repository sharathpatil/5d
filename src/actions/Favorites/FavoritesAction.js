export const addToFavorites = data => {
  return {
    type: 'ADD_TO_FAVORITES',
    payload: data,
  };
};

export const removeFromFavorites = data => {
  return {
    type: 'REMOVE_FROM_FAVORITES',
    payload: data,
  };
};