import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';

const {width, height} = Dimensions.get('screen');

const CharacterCard = props => {
  return (
    <View style={styles.card}>
      <Image source={{uri: props.image}} style={styles.cardImg} />

      <View style={styles.cardContent}>
        <View style={{flex: 1, marginVertical: 6}}>
          <Text style={styles.cardTitle}>{props.title}</Text>
          <Text style={styles.cardSubTitle}>{props.subTitle}</Text>
        </View>

        <TouchableOpacity
          onPress={() => {
            props.onFavPressed();
          }}>
          <Image
            source={
              props.isFav
                ? require('../assets/images/fav-fill.png')
                : require('../assets/images/fav.png')
            }
            style={styles.favIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CharacterCard;

const fontBold = 'Roboto-Bold';
const fontLight = 'Roboto-Light';
const fontThin = 'Roboto-Thin';

const styles = StyleSheet.create({
  card: {
    width: width / 2 - 24,
    marginHorizontal: 12,
    marginTop: 12,
  },

  cardImg: {
    width: width / 2 - 24,
    height: width / 1.9,
    backgroundColor: '#ffffff20',
    borderRadius: 6,
  },

  cardContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  cardTitle: {
    fontSize: 14,
    fontFamily: fontBold,
    color: '#fff',
  },

  cardSubTitle: {
    fontSize: 12,
    fontFamily: fontLight,
    color: '#fff',
  },

  favIcon: {
    width: 22,
    height: 22,
  },
});
