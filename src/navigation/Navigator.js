import React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeContainer from '../screens/Home/HomeContainer';
import FavoritesContainer from '../screens/Favorites/FavoritesContainer';
import SearchContainer from '../screens/Search/SearchContainer';

const Stack = createNativeStackNavigator();

const Navigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
      
        initialRouteName="HomeContainer"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="HomeContainer" component={HomeContainer} />

        <Stack.Screen
          name="FavoritesContainer"
          component={FavoritesContainer}
        />

        <Stack.Screen name="SearchContainer" component={SearchContainer} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
